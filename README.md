# iAsst
Chrome插件，帮助你使用Chrome

## 背景
1. 好多同类的插件，提供的服务都是国外服务器，很容易访问失败，而且网络速度非常慢。
2. 好多插件已经不提供更新，没法使用。

## 功能
1. 稍候阅读
2. 右键查字典

## 架构
1. chrome插件，实现前端的展示
2. web服务，提供数据存储

## 版权声明
iAsst使用 [GNU General Public License v2.0](http://www.gnu.org/licenses/gpl-2.0.txt) 协议

## 参与者
[hyhjwzx](http://git.oschina.net/hyhjwzx)